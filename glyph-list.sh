#!/usr/bin/env bash

# Iterate through relevant files in nerd-fonts
for i in $(dirname "${BASH_SOURCE[0]:-$0}")/nerd-fonts/bin/scripts/lib/i_{dev,fae,fa,iec,linux,oct,ple,pom,seti,material,weather}.sh; do
  # Transform to <unicodeChar,name_with_underscores>
  sed -n -r "s/^\s*i='(.*)'\s*i_(.*)=.*/\1,\2/p" $i |
  while IFS=',' read -r -a line;
  do
    # Get character code and space delimit the name
    printf '%x %s\n' "'${line[0]}" "${line[1]//_/ }"
  done
done \
> glyph-list.txt
